//adding new row
$('.add-button').click(function () {
    $(".task__table>tbody").append(`<tr class="tr">
    <td>
        <label>Factor
            <input type="text">
          </label>
    </td>
    <td >
        <label>How important this factor is?
            <select class="importance">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </label>
    </td>
    <td>
        <label>compatibility degree
            <select class="importanceA">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </label>
    </td>
    <td>
        <label>compatibility degree
            <select class="importanceB">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </label>
    </td>
    <td class="remove">Remove</td>
</tr>`);
});

//removing row
$('body').on('click', '.remove', function (e) {
    e.target.closest('tr').remove();
});

//Result

// let rows = document.querySelectorAll('tr');



function count() {
    setInterval(() => {
        let rows = document.querySelectorAll('tr');
        let sumA = 0;
        let sumB = 0;
        let max = 0;
        
        for(let i = 1; i < rows.length; i++) {
            let row = rows[i];

            let oneRow = row.getElementsByTagName('td');
            let impA = oneRow.item(1).querySelector('select').value * oneRow.item(2).querySelector('select').value
            sumA += impA;
            let impB = oneRow.item(1).querySelector('select').value * oneRow.item(3).querySelector('select').value
            sumB += impB;
            
            max += oneRow.item(1).querySelector('select').value *5;

        
        }
    

        let aPercent = Math.round(sumA*100/max);
        let bPercent = Math.round(sumB*100/max);
    
        let advice = '';
        if (aPercent>bPercent) {
            advice = 'Option A is better';
        } else if (bPercent>aPercent) {
            advice = 'Option B is better';
        } else {
            advice = "Both options are similar. Try add some new factors to make decision easier:) "
        }
    
        document.getElementById('a_score').innerHTML = `Option A meets your goal in <span class="blue" >${aPercent}</span>%`;
        document.getElementById('b_score').innerHTML = `Option B meets your goal in <span class="blue" >${bPercent}</span>%`;
        document.getElementById('advice').innerText = advice;
    
    }, 1);
}


$(document).ready(count);